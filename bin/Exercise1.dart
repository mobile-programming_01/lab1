import 'dart:io';

import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;

void main() {
  print("MENU");
  print("Select the choice you want to perform");
  print("1. ADD");
  print("2. SUBTRACT");
  print("3. MULTIPLY");
  print("4. DIVIDE");
  print("5. EXIT");
  print("Choice you want to enter : ");
  var choice = (stdin.readLineSync()!);

  switch (choice) {
    case "1":
      {
        print('Enter the value for x : ');
        int x = int.parse(stdin.readLineSync()!);
        print('Enter the value for y : ');
        int y = int.parse(stdin.readLineSync()!);
        print('Sum of the two numbers is : ');
        add(x, y);
      }
      break;
    case "2":
      {
        print('Enter the value for x : ');
        int x = int.parse(stdin.readLineSync()!);
        print('Enter the value for y : ');
        int y = int.parse(stdin.readLineSync()!);
        print('Sum of the two numbers is : ');
        sub(x, y);
      }
      break;
    case "3":
      {
        print('Enter the value for x : ');
        int x = int.parse(stdin.readLineSync()!);
        print('Enter the value for y : ');
        int y = int.parse(stdin.readLineSync()!);
        print('Sum of the two numbers is : ');
        mul(x, y);
      }
      break;
    case "4":
      {
        print('Enter the value for x : ');
        int x = int.parse(stdin.readLineSync()!);
        print('Enter the value for y : ');
        int y = int.parse(stdin.readLineSync()!);
        print('Sum of the two numbers is : ');
        divi(x, y);
      }
      break;
    case "5":
      {
        print("Exit Program Exercise1.");
      }
      break;
  }
}

void add(int x, int y) {
  int sum = x + y;

  print('$sum');
}

void sub(int x, int y) {
  int sum = x - y;

  print('$sum');
}

void mul(int x, int y) {
  int sum = x * y;

  print('$sum');
}

void divi(int x, int y) {
  double sum = x / y;

  print('$sum');
}